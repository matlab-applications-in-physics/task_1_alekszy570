% Matlab R2020b script
% name: moj_kalkulator.m
% author: alekszy570
% date: 2020-11-14
% version: v1
%zad1

Planck_Const=6,63*10^(-34)         % defining a constant
zad1=Planck_Const/(2*pi)          %calculations

%zad2
zad2=sin(pi/4/exp(1))             %calculations

%zad3
zad3=hex2dec('0X0098D6')/(1.445*10^23)  % hex2dec converts the hexadecimal the decimal number

%zad4
zad4=sqrt(exp(1)-pi)            %calculations

%zad5
zad5=floor(pi*10^12)-floor(pi*10^11)*10  %floor round down

%zad6
zad6=abs(datenum('25-Apr-2020 05:00:00') - datenum('9-Nov-2020 12:00:00')) %abs returns the absolute value, datenum convert date to number

%zad7
Earth_Radius=6371000               % defining a constant

zad7=atan((exp(sqrt(7)/2)-log(R/(10^8))/log(exp(1)))/hex2dec('0XAAFF'))  %atan inverse tangent in radians

%zad8
Number_Atoms=6.02214076*10^23/10^6/4          % calculations and defining a constant

%zad9
zad9=(2/9*(Number_Atoms))/(2/9*(Number_Atoms))*1000/101  % 2/9 two out of nine carbon atoms in the molecule, 1000 convert per mille, 101 all atoms of carbon



